
import Vue from 'vue'
import App from './App'
import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false

let vm = new Vue({
  el: '#app',
  render: h => h(App)
})

Vue.use({
  vm
})
